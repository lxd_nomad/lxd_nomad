packer {
  required_plugins {
    lxd = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/lxd"
    }
  }
}

source "lxd" "base-server" {
  image         = "ubuntu-minimal:jammy"
  output_image  = "base-server"
  publish_properties = {
    description = "base image for hashicorp based instances"
  }
}

build {
  sources = ["source.lxd.base-server"]
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    inline = [
      "echo Acquire::Languages \"none\"\\; > /etc/apt/apt.conf.d/99translations",
      "mkdir /root/.gnupg",
      "chmod 700 /root/.gnupg",
      "wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /etc/apt/trusted.gpg.d/hashicorp-archive-keyring.gpg",
      "echo 'deb https://apt.releases.hashicorp.com jammy main' > /etc/apt/sources.list.d/hashicorp.list",
      "gpg --no-default-keyring --keyring /usr/share/keyrings/neovim-ppa-ubuntu.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 9DBB0BE9366964F134855E2255F96FCF8231B6DD",
      "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/neovim-ppa-ubuntu.gpg] https://ppa.launchpadcontent.net/neovim-ppa/unstable/ubuntu jammy main' > /etc/apt/sources.list.d/neovim-ppa.list",
      "apt update",
      "apt install neovim consul -yqq",
      "apt full-upgrade -yqq --autoremove --purge",
      "apt clean",
    ]
  }
}

source "lxd" "consul-server" {
  image = "local:base-server"
  output_image = "consul-server"
  publish_properties = {
    description = "base image for consul server"
  }
}

build {
  sources = ["source.lxd.consul-server"]
}

source "lxd" "nomad-server" {
  image = "local:consul-server"
  output_image = "nomad-server"
  publish_properties = {
    description = "base image for nomad server"
  }
}

build {
  sources = ["source.lxd.nomad-server"]
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    inline = [
      "apt install nomad -yqq",
      "apt clean"
    ]
  }
}

source "lxd" "client-server" {
  image = "local:nomad-server"
  output_image = "client-server"
  publish_properties = {
    description = "base image for nomad client"
  }
}

build {
  sources = ["source.lxd.client-server"]
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    inline = [
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" > /etc/apt/sources.list.d/docker.list",
      "apt update",
      "apt install docker-ce docker-ce-cli -yqq"
    ]
  }

}

source "lxd" "vault-server" {
  image = "local:consul-server"
  output_image = "vault-server"
  publish_properties = {
    description = "base image for nomad server"
  }
}

build {
  sources = ["source.lxd.vault-server"]
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    inline = [
      "apt install vault -yqq",
      "apt clean"
    ]
  }
}
