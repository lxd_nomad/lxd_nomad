variable "cert" {
  default = "~/.ssh/id_rsa.pub"
}

variable "consul_servers" {
  default = 3
}

variable "vault_servers" {
  default = 2
}

variable "nomad_servers" {
  default = 3
}

variable "client_servers" {
  default = 3
}
