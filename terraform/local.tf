resource "null_resource" "consul_instances" {
  triggers = {
    consul_servers = join(",", lxd_instance.consul.*.status),
  }

  provisioner "local-exec" {
    command = <<-EOT
      lxc exec consul${count.index + 1} -- cloud-init status --wait > /dev/null
     EOT
  }

  count = var.consul_servers
}

resource "null_resource" "vault_instances" {
  triggers = {
    consul_servers = join(",", lxd_instance.vault.*.status),
  }

  provisioner "local-exec" {
    command = <<-EOT
      lxc exec vault${count.index + 1} -- cloud-init status --wait > /dev/null
    EOT
  }

  count = var.vault_servers
}

resource "null_resource" "nomad_instances" {
  triggers = {
    nomad_servers = join(",", lxd_instance.nomad.*.status),
  }

  provisioner "local-exec" {
    command = <<-EOT
      lxc exec nomad${count.index + 1} -- cloud-init status --wait > /dev/null
    EOT
  }

  count = var.nomad_servers
}

resource "null_resource" "client_instances" {
  triggers = {
    client_servers = join(",", lxd_instance.client.*.status),
  }

  provisioner "local-exec" {
    command = <<-EOT
      lxc exec client${count.index + 1} -- cloud-init status --wait > /dev/null
    EOT
  }

  count = var.client_servers
}
