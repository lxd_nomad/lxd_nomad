resource "lxd_profile" "docker" {
  name = "docker"

  config = {
    "security.nesting"=true
    "security.syscalls.intercept.mknod"=true
    "security.syscalls.intercept.setxattr"=true
  }
}

resource "lxd_profile" "consul" {
  name = "consul"

  config = {
    "boot.autostart" = true
    "cloud-init.user-data" = templatefile("files/consul_cloud_config.tftpl", {
      key             = file(var.cert),
      consul_gossip   = base64encode(random_password.consul_gossip.result),
      consul_servers  = var.consul_servers
    })
  }
}

resource "lxd_profile" "vault" {
  name = "vault"

  config = {
    "boot.autostart" = true
    "cloud-init.user-data" = templatefile("files/vault_cloud_config.tftpl", {
      key             = file(var.cert),
      consul_gossip   = base64encode(random_password.consul_gossip.result),
      consul_servers  = var.vault_servers
    })
    "environment.VAULT_ADDR"="http://127.0.0.1:8200"
  }
}

resource "lxd_profile" "nomad" {
  name = "nomad"

  config = {
    "boot.autostart" = true
    "cloud-init.user-data" = templatefile("files/nomad_cloud_config.tftpl", {
      key             = file(var.cert)
      consul_gossip   = base64encode(random_password.consul_gossip.result),
      consul_servers  = var.consul_servers,
      nomad_gossip    = base64encode(random_password.nomad_gossip.result),
    })
    "environment.NOMAD_ADDR"="https://127.0.0.1:4646"
    "environment.NOMAD_CACERT"="/etc/nomad.d/nomad_certs/nomad-agent-ca.pem"
    "environment.NOMAD_CLIENT_CERT"="/etc/nomad.d/nomad_certs/global-cli-nomad-0.pem"
    "environment.NOMAD_CLIENT_KEY"="/etc/nomad.d/nomad_certs/global-cli-nomad-0-key.pem"
  }
}

resource "lxd_profile" "client" {
  name = "client"

  config = {
    "boot.autostart" = true
    "cloud-init.user-data" = templatefile("files/client_cloud_config.tftpl", {
      key             = file(var.cert),
      consul_gossip   = base64encode(random_password.consul_gossip.result),
      consul_servers  = var.consul_servers
    })
    "environment.NOMAD_ADDR"="https://127.0.0.1:4646"
    "environment.NOMAD_CACERT"="/etc/nomad.d/nomad_certs/nomad-agent-ca.pem"
    "environment.NOMAD_CLIENT_CERT"="/etc/nomad.d/nomad_certs/global-cli-nomad-0.pem"
    "environment.NOMAD_CLIENT_KEY"="/etc/nomad.d/nomad_certs/global-cli-nomad-0-key.pem"
  }
}
