resource "lxd_instance" "client" {

  depends_on = [
    lxd_instance.consul,
    lxd_instance.nomad,
  ]

  name      = format("client%d", count.index + 1)
  image     = "ubuntu-minimal:jammy"
  ephemeral = false

  profiles  = [
    "default",
    lxd_profile.docker.name,
    lxd_profile.client.name,
  ]

  limits = {
    cpu = 4
    memory = "4GB"
  }

  provisioner "local-exec" {
    command = "lxc file push --recursive --quiet ./consul_certs client${count.index + 1}/tmp"
  }

  provisioner "local-exec" {
    command = "lxc file push --recursive --quiet ./nomad_certs client${count.index + 1}/tmp"
  }

  count = var.client_servers
}
