resource "random_password" "consul_gossip" {
  length = 32
}

resource "lxd_instance" "consul" {

  name      = format("consul%d", count.index + 1)
  image     = "ubuntu-minimal:jammy"
  ephemeral = false

  profiles  = [
    "default",
    lxd_profile.consul.name,
  ]

  limits = {
    cpu = 1
    memory = "1GB"
  }

  provisioner "local-exec" {
    command = <<-EOT
              if [ ${count.index} = 0 ]; then
                lxc exec consul${count.index + 1} -- cloud-init status --wait > /dev/null
                lxc file pull --recursive --quiet consul${count.index + 1}/etc/consul.d/consul_certs ./
              fi
    EOT
  }

  provisioner "local-exec" {
    when    = destroy
    command = "if [ ${count.index} = 0 ]; then { rm -rf ./consul_certs; } fi"
  }

  provisioner "local-exec" {
    command = <<-EOT
              if [ ${count.index} != 0 ]; then
                while [ ! -f ./consul_certs/CONSUL_CERTS_CREATED ]; do
                  sleep 1
                done
                lxc file push --recursive --quiet ./consul_certs consul${count.index + 1}/tmp
              fi
    EOT
  }

  count = var.consul_servers
}
