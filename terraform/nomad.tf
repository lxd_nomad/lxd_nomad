resource "random_password" "nomad_gossip" {
  length = 32
}

resource "lxd_instance" "nomad" {

  depends_on = [
    lxd_instance.consul
  ]

  name      = format("nomad%d", count.index + 1)
  image     = "ubuntu-minimal:jammy"
  ephemeral = false

  profiles  = [
    "default",
    lxd_profile.nomad.name,
  ]

  limits = {
    cpu = 1
    memory = "1GB"
  }

  provisioner "local-exec" {
    command = "lxc file push --recursive --quiet ./consul_certs nomad${count.index + 1}/tmp"
  }

  provisioner "local-exec" {
    command = <<-EOT
              if [ ${count.index} = 0 ]; then
                lxc exec nomad${count.index + 1} -- cloud-init status --wait > /dev/null
                lxc file pull --recursive --quiet nomad${count.index + 1}/etc/nomad.d/nomad_certs ./
              fi
    EOT
  }

  provisioner "local-exec" {
    when    = destroy
    command = "if [ ${count.index} = 0 ]; then { rm -rf ./nomad_certs; } fi"
  }

  provisioner "local-exec" {
    command = <<-EOT
              if [ ${count.index} != 0 ]; then
                while [ ! -f ./nomad_certs/NOMAD_CERTS_CREATED ]; do
                  sleep 1
                done
                lxc file push --recursive --quiet ./nomad_certs nomad${count.index + 1}/tmp
              fi
    EOT
  }

  count = var.nomad_servers
}
