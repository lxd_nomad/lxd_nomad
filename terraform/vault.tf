resource "lxd_instance" "vault" {

  depends_on = [
    null_resource.consul_instances
  ]

  name      = format("vault%d", count.index + 1)
  image     = "ubuntu-minimal:jammy"
  ephemeral = false

  profiles  = [
    "default",
    lxd_profile.vault.name,
  ]

  limits = {
    cpu = 1
    memory = "1GB"
  }

  provisioner "local-exec" {
    command = "lxc file push --recursive --quiet ./consul_certs vault${count.index + 1}/tmp"
  }

  count = var.vault_servers
}
